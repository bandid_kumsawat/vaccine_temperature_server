var router = require('express').Router();
var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Device = mongoose.model('Device');
var Event = mongoose.model('Event');
var auth = require('../auth');

var moment = require('moment')
var momentTz = require('moment-timezone')


router.get('/',function(req,res,next){

    var qry = {}   
    
    if(req.param.id !== undefined){
        qry.id = req.param.id;
    }

    if(req.param.start !== undefined){
        qry.start = req.param.start;
    }
    
    if(req.param.start !== undefined){
        qry.stop = req.param.stop;
    }

    if(req.param.date !== undefined){
        qry.stop = req.param.stop;
    }

    console.log(req.query.ed);
    
    Event.find(qry).then(function(ret){
        console.log(ret)
        return res.json(ret);
      }).catch(next);
});


router.post('/',function(req,res,next){

    if(!req.body.start){
        return res.status(422).json({errors: {start: "can't be blank"}});
    }
    
    if(!req.body.stop){
        return res.status(422).json({errors: {stop: "can't be blank"}});
    }
    

    var st_time = moment(req.body.start)    
    //, 'YYYY-MM-DDTHH:MM:SS')
    var ed_time = moment(req.body.stop) 
    // , 'YYYY-MM-DDTHH:MM:SS')
    console.log(st_time)
    console.log(ed_time)
    
    try {
        if(!st_time.isValid()){
            return res.status(422).json({errors: {start: "Fail"}});
        }
        if(!ed_time.isValid()){
            return res.status(422).json({errors: {stop: "Fail"}});
        }
    } catch (error) {
        console.log(error);
    }

    Event.findOne({}).sort({id:-1}).then(function(ret){
        // console.log(ret)
        var evt = new Event();
        
        if(ret === null){
            evt.id="EVT000001";
        }else {
            var id_srt = ret.id
            id_srt = id_srt.substring(id_srt.length-6 ,id_srt.length)
            console.log(id_srt)
            var str = "" + (parseInt(id_srt) +1)
            var pad = "000000"
            var id = pad.substring(0, pad.length - str.length) + str
            console.log(id_srt);
            console.log("EVT"+id);
            evt.id = "EVT"+id;
        }
        console.log(st_time.format("YYYY-MM-DD"))
        evt.date = st_time.format("YYYY-MM-DD")
        evt.start = req.body.start
        evt.stop  = req.body.stop
        evt.save().then(function(){
            return res.json({ret:"ok"});
        }).catch(next);
    });

});

router.post('/update',function(req,res,next){
    var check_para = {};

    if (req.body.id ===undefined){
        return res.status(422).json({errors: {start: "Fail Please insert ID"}});
    }

    if (req.body.start !==undefined){
      check_para.start = req.body.start;
    }

    if (req.body.stop !==undefined){
      check_para.stop = req.body.stop;
    }

    if (req.body.unit !==undefined){
        check_para.unit = req.body.unit;
      }

    // console.log(req.body.Unit)



    var update_json      = {$set:check_para}

    Event.findOneAndUpdate({id:req.body.id}, update_json,{new: true}, (err, doc) => {
        if (err) {
            console.log("Something wrong when updating data!");
            return res.json({ret:"fail"});
        }
        return res.json(doc);
    });

});


router.delete('/',function(req,res,next){
    console.log(req.body.id);
    if(typeof req.body.id === 'undefined'){
      return res.status(422).json({error:"no device_id"})
    }

    Event.remove({ id: req.body.id }, function(err) {
      if (!err) {
        return res.json({ret:"delete Success "+req.body.id});
      }
      else {
        return res.json({ret:"delete fail"});
      }
    });
    
});

module.exports = router;