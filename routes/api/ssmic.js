var router = require('express').Router();
var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Device = mongoose.model('Device');
var auth = require('../auth');

var moment = require('moment')
var momentTz = require('moment-timezone')

router.post('/start', function(req, res, next) {
    
    var check_para = {stream:1};

    if (req.body.id ===undefined){
        return res.json({ret:"fail"});
    }


    var update_json      = {$set:check_para}

    var len = req.body.id.length

    // return res.json({ret:req.body.id.length});
    for(i=0;i<len;i++){
          Device.findOneAndUpdate({id:req.body.id[i]}, update_json,{new: true}, (err, doc) => {

      if (err) {
          console.log("Something wrong when updating data!");
      }
        });
    }

      return res.json({ret:"OK"});


});

// update device
router.post('/stop', function(req, res, next) {

    var check_para = {stream:0};

    if (req.body.id ===undefined){
        return res.json({ret:"fail"});
    }


    var update_json      = {$set:check_para}

    var len = req.body.id.length

    // return res.json({ret:req.body.id.length});
    for(i=0;i<len;i++){
          Device.findOneAndUpdate({id:req.body.id[i]}, update_json,{new: true}, (err, doc) => {

      if (err) {
          console.log("Something wrong when updating data!");

      }
        });
    }

      return res.json({ret:"OK"});

});




module.exports = router;