var router = require('express').Router();
var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Device = mongoose.model('Device');
var auth = require('../auth');

var moment = require('moment')
var momentTz = require('moment-timezone')

router.get('/', function(req, res, next) {
    Device.find({}).then(function(ret){
      console.log(ret)
      return res.json(ret);
    }).catch(next);
  });

router.post('/add', function(req, res, next) {
    
    var dev = new Device();

    // dev.id = req.body.device.id;
    dev.lat = ""
    dev.long = ""
    dev.stream = 0
    dev.info =""
    dev.offset = "0.8"
    dev.temp = "0.0"
    Device.find({}).sort({
      id: -1 
     }).then(function(result,err){
     
  
      var id_srt = result[0].id
  
      id_srt = id_srt.substring(id_srt.length-6 ,id_srt.length)
  
  
      var str = "" + (parseInt(id_srt) +1)
      var pad = "000000"
      var id = pad.substring(0, pad.length - str.length) + str
  
      console.log(id_srt);
      console.log("WS"+id);
      dev.id = "WS"+id;
      if (req.body.device.info !=='undefined'){
        dev.info = req.body.device.info;
      }
      
      dev.save().then(function(){
        return res.json({ret:"ok"});
      }).catch(next);


  
     });


 });

// update device
router.post('/update', function(req, res, next) {

    var check_para = {};

    console.log(req.body)

    if (req.body.temp !== undefined){
      check_para.temp = req.body.temp;
    }

    if (req.body.batt !==undefined){
      check_para.batt = req.body.batt;
    }

    if (req.body.lat !==undefined){
      check_para.lat = req.body.lat;
    }

    if (req.body.long !==undefined){
      check_para.long = req.body.long;
    }

    if (req.body.info !==undefined){
      check_para.info = req.body.info;
    }

    if (req.body.stream !==undefined){
      check_para.stream = req.body.stream;
    }

    if (req.body.gps_count !==undefined){
      check_para.gps_count = req.body.gps_count;
    }

    if (req.body.tel !==undefined){
      check_para.tel = req.body.tel;
    }

    if (req.body.tel_op !==undefined){
      check_para.tel_op = req.body.tel_op;
    }

    if (req.body.offset !==undefined){
      check_para.offset = req.body.offset;
    }

    var nowtime = moment().toISOString(true);//moment(new Date()).zone('+0700').toISOString()
    console.log(nowtime);

    var dd = new Date().toISOString()
    console.log(dd);
    
    check_para.timestamp = nowtime;

    var update_json      = {$set:check_para}

    Device.findOneAndUpdate({id:req.body.id}, update_json,{new: true}, (err, doc) => {

      if (err) {
          console.log("Something wrong when updating data!");
          return res.json({ret:"fail"});
      }
      return res.json({ret:"OK"});

      // console.log(doc);
    });

});

// Delete Device

router.delete('/',  function(req, res, next) {

    console.log(req.body.id);
    
    if(typeof req.body.id === 'undefined'){
      return res.status(422).json({error:"no device_id"})
    }

    Device.remove({ id: req.body.id }, function(err) {
      if (!err) {
        return res.json({ret:"delete Success"});
      }
      else {
        return res.json({ret:"delete fail"});
      }
    });
});



module.exports = router;