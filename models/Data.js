var mongoose = require('mongoose');


var DataSchema = new mongoose.Schema({
    id: String,
    ch_count :Number,
    sampling :Number,
    payload : Array ,
    UTC : Number,
    Time : String,
    gps_count:Number,
  },{ collection : 'data' });


  mongoose.model('Data', DataSchema);
  


  