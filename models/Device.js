var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var DeviceSchema = new mongoose.Schema({
    id: {type: String, unique: true},
    lat :String,
    long :String,
    info :String,
    batt :String,
    gps_count:Number,
    stream:Number,
    tel:String,
    tel_op:String,
    Schedule:[Object],
    timestamp:String,
    offset: String,
    temp: String
  });

  DeviceSchema.plugin(uniqueValidator, {message: 'is already taken.'});


  DeviceSchema.methods.toJSONFor = function(user){
    return {
      lat :this.lat,
      long :this.long,
      info :this.info,
      batt :this.batt,
      Schedule:this.Schedule
    };
  };

  mongoose.model('Device', DeviceSchema);
  
