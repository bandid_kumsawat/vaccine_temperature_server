var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var EventSchema = new mongoose.Schema({
    id:{type:String,unique:true},
    start:String,
    stop:String,
    date:String,
    st_time:String,
    ed_time:String,
    unit:[String]
  });

EventSchema.plugin(uniqueValidator, {message: 'is already taken.'});


EventSchema.methods.favorite = function(id){
  console.log(id);
  if(this.Unit.indexOf(id) === -1){
    this.Unit.push(id);
  }

  return this.save();
};

mongoose.model('Event', EventSchema);